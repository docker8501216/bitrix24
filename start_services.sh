#!/bin/bash

# Start named (DNS server)
named -g &

# Start Apache HTTPD
httpd &

# Start Nginx
nginx &

# Keep the container running
tail -f /dev/null
